
import pandas as pd
from datetime import datetime

#In our case, we have deleted duplicates (line 73 to 80), so the number of items in the list corresponds to the number of customers, foods and drinks
#Transform the date format  in the CSV file into a real datetime python type
def transforme_date(date_str):
    date_format = "%d/%m/%y %H:%M"
    return datetime.strptime(date_str, date_format)

def imprimer_data(customers, drinks, foods, total_foods, total_drinks, vente_heures):
    global food, total, drink, heure
# The len() function is used to obtain the size of a list
    nombre_clients = len(customers)
#The join () function (applied on a string) allows to attach the elements of the list with each time this string between the elements
    boissons = "\n\t- ".join([d for d in drinks if str(d) != 'non'])
    nourritures = "\n\t- ".join([f for f in foods if str(f) != 'non'])
    print("Part 1")
    print("Number of customers: %s" % nombre_clients)
    print("Foods:\n\t- %s" % nourritures)
    print("Drinks:\n\t- %s" % boissons)
    print("Part 2")
    print("Total nourriture vendue:")
#On a dictionary, the items () function is used to obtain a list with the key and the value together.

    for food, total in total_foods.items():
        print("\t- %s\t%s" % (food, total))
    print("Total boissons vendues")
    for drink, total in total_drinks.items():
        print("\t- %s\t%s" % (drink, total))
    print("Part 3")
    for heure, sous_dict in vente_heures.items():
        print("%s" % heure)
        print("Part 3")

        total = float(sous_dict.pop('Total', 1))
        for type_article, articles in sous_dict.items():
            print("%s" % type_article)
            for article, nb in articles.items():
                print("\t%s: %.2f%%" % (article, nb / total * 100))

def main():
    foods = []
    drinks = []
    customers = []

#We count the number of foods and we put that in a dictionary where key = food and value = number
    total_foods = {}
    total_drinks = {}
    total_customers = {}

#Sales/hours
    vente_heures = {}
#Execute a sequence of statements multiple times and abbreviates the code that manages the loop variable
#We use a loop here on each element of the list (each element is a dictionnary)
    cpt = 0

    chemin = "Coffeebar_2013-2017.csv"
    df = pd.read_csv(chemin, sep=';')
    lignes = df.to_dict('records')
   #To_dict is used to read each line of our file like a dictionnary
    for ligne_dict in lignes:
        cpt += 1
        print("Traitement ligne %s" % cpt)

        food = ligne_dict.get('FOOD', '')
        if str(food) == 'nan':
            food = 'Rien'
        drink = ligne_dict.get('DRINKS', '')
        if str(drink) == 'nan':
            drink = 'Rien'
        customer = ligne_dict.get('CUSTOMER', '')
        heure = ligne_dict.get('TIME', '')

        if food not in foods:
            foods.append(food)

        if drink not in drinks:
            drinks.append(drink)

        if customer not in customers:
            customers.append(customer)

        total_food = total_foods.get(food, 0) + 1
        total_drink = total_drinks.get(drink, 0) + 1
        total_customer = total_customers.get(customer, 0) + 1

        total_foods.update({
            food: total_food,
        })
        total_drinks.update({
            drink: total_drink,
        })
        total_customers.update({
            customer: total_customer,
        })


        heure_format = "%H:%M"
        time = transforme_date(heure)
        heure = time.strftime(heure_format)

        vente_cette_heure = vente_heures.get(heure, {})

        vente_cette_heure_food = vente_cette_heure.get('foods', {})
        vente_cette_heure_drink = vente_cette_heure.get('drinks', {})

        total_drink = vente_cette_heure_drink.get(drink, 0) + 1
        total_food = vente_cette_heure_food.get(food, 0) + 1

        vente_cette_heure_drink.update({
            drink: total_drink,
        })
        vente_cette_heure_food.update({
            food: total_food,
        })
        total = vente_cette_heure.get('Total', 0) + 1
        vente_cette_heure.update({
            'foods': vente_cette_heure_food,
            'drinks': vente_cette_heure_drink,
            'Total': total,
        })
        vente_heures.update({
            heure: vente_cette_heure,
        })
    imprimer_data(customers, drinks, foods, total_foods, total_drinks, vente_heures)



if __name__ == "__main__":
    main()

